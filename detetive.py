print('Digite 1 para sim e 0 para não')

perguntas = []

perguntas.append(int(input('Telefonou para vitima?')))
perguntas.append(int(input('Esteve no local do crime?')))
perguntas.append(int(input('Mora perto da vitima?')))
perguntas.append(int(input('Devia para vitima?')))
perguntas.append(int(input('Já trabalhou com a vitima?')))

quantidade_acertos = sum(perguntas)

if quantidade_acertos == 2:
  print('Suspeita')
elif quantidade_acertos >= 3 and quantidade_acertos <= 4:
  print('Cúmplice')
elif quantidade_acertos == 5:
  print('Assassino')
else:
  print('Inocente')
